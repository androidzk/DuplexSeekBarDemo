#效果图
![输入图片说明](http://git.oschina.net/uploads/images/2016/0505/140151_43273511_312885.png "在这里输入图片标题")
#代码示例
```
<org.bear.widget.DuplexSeekBar
        android:id="@+id/price_slider"
        android:layout_width="match_parent"
        android:layout_height="wrap_content"
        android:minHeight="96dp"
        app:insideRangeLineColor="@color/colorAccent"
        app:insideRangeLineStrokeWidth="4dp"
        app:outsideRangeLineColor="#787878"
        app:outsideRangeLineStrokeWidth="2dp"
        app:pressedTargetRadius="12dp"
        app:targetColor="@color/colorPrimaryDark"
        app:unpressedTargetRadius="6dp" />
```