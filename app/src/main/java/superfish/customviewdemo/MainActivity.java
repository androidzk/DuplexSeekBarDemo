package superfish.customviewdemo;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.edmodo.rangebar.RangeBar;

import org.bear.widget.EasyLinearLayout;

public class MainActivity extends BaseActivity {

    private RangeBar bar;

    private TextView rTv;

    private EasyLinearLayout easyLl;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        bar = (RangeBar) findViewById(R.id.rangeBar);
        rTv = (TextView) findViewById(R.id.rTv);
        easyLl = (EasyLinearLayout) findViewById(R.id.easyLl);
        bar.setOnRangeBarChangeListener(new RangeBar.OnRangeBarChangeListener() {
            @Override
            public void onIndexChangeListener(RangeBar rangeBar, int i, int i1) {
                Log.e("i==", i + "");
                Log.e("i1==", i1 + "");
                rTv.setText("从" + i + "到" + i1);
            }
        });
        easyLl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, ScrollingActivity.class);
                startActivity(intent);
            }
        });
    }
}
