package org.bear.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;
import android.os.Build;
import android.support.annotation.ColorInt;
import android.util.AttributeSet;
import android.widget.Button;

/**
 * 简单设置点击色的控件
 *
 * @author 大熊
 */
public class EasyButton extends Button {
    private int pressColor;

    public EasyButton(Context context) {
        super(context);
        init(null);
    }

    public EasyButton(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(attrs);
    }

    public EasyButton(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(attrs);
    }

    public void init(AttributeSet attrs) {
        if (attrs != null) {
            TypedArray typedArray = getContext().obtainStyledAttributes(attrs, R.styleable.EasyView, 0, 0);
            pressColor = typedArray.getColor(R.styleable.EasyView_pressColor, -1);
            typedArray.recycle();
        }
    }

    @Override
    public boolean isInEditMode() {
        return true;
    }

    @Override
    public void setBackground(Drawable background) {
        //如果没有默认色 不处理
        if (pressColor == -1) {
            super.setBackground(background);
            return;
        }
        EasyRelativeLayoutBackgroundDrawable layer = new EasyRelativeLayoutBackgroundDrawable(
                background);
        super.setBackground(layer);
    }

    /**
     * 设置按下的颜色
     *
     * @param color
     */
    public void setPressColor(@ColorInt int color) {
        this.pressColor = color;
    }

    protected class EasyRelativeLayoutBackgroundDrawable extends LayerDrawable {
        protected int _disabledAlpha = 100;

        public EasyRelativeLayoutBackgroundDrawable(Drawable d) {
            super(new Drawable[]{d});
        }

        @Override
        protected boolean onStateChange(int[] states) {
            boolean enabled = false;
            boolean pressed = false;
            for (int state : states) {
                if (state == android.R.attr.state_enabled)
                    enabled = true;
                else if (state == android.R.attr.state_pressed)
                    pressed = true;
            }
            mutate();
            if (enabled && pressed) {
                setColor(pressColor);
            } else if (!enabled) {
                setColorFilter(null);
                setAlpha(_disabledAlpha);
            } else {
                setColorFilter(null);
            }
            invalidateSelf();
            return super.onStateChange(states);
        }

        private void setColor(@ColorInt int color) {
            PorterDuff.Mode mode = PorterDuff.Mode.SRC_IN;
            if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.GINGERBREAD_MR1) {
                mode = PorterDuff.Mode.MULTIPLY;
            }
            if (EasyButton.this.getBackground() != null)
                EasyButton.this.getBackground().setColorFilter(color, mode);
        }

        @Override
        public boolean isStateful() {
            return true;
        }
    }


}
